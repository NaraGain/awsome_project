hello there 

#prepare for create ci/cd tool in gitlab

#use ci/cd tools for deploy image in docker hub


#build from ubuntu image
FROM ubuntu:latest

# Set non-interactive mode during build
ENV DEBIAN_FRONTEND=noninteractive

# Install required dependencies for Frappe Framework
RUN apt-get update \
    && apt-get install -y \
        python3 \
        python3-pip \
        build-essential \
        libssl-dev \
        libffi-dev \
        libmysqlclient-dev \
        libjpeg8-dev \
        liblcms2-dev \
        libblas-dev \
        liblapack-dev \
        libatlas-base-dev \
        libproj-dev \
        wkhtmltopdf \
        nodejs \
        npm \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Upgrade pip and install Frappe Bench
RUN pip3 install --upgrade pip \
    && pip3 install frappe-bench

# Expose ports for Frappe Bench
EXPOSE 8000 9000

# Set the working directory
WORKDIR /home/frappe

# Create a new Frappe Bench
RUN useradd -m frappe \
    && su - frappe && apt-get install "bench init frappe-bench --frappe-branch version-13"

# Switch to the Frappe Bench directory
WORKDIR /home/frappe/frappe-bench

# Copy local Frappe project into the container
COPY ./myapp /home/frappe/frappe-bench

# Start Frappe Bench
CMD ["bench", "start"]


#image 2

# FROM ubuntu:latest

# # Set non-interactive mode during build
# ARG DEBIAN_FRONTEND=noninteractive

# # Install dependencies
# RUN apt-get update && \
#     apt-get install -y \
#         git \
#         curl \
#         wget \
#         build-essential \
#         libssl-dev \
#         libffi-dev \
#         libmysqlclient-dev \
#         libjpeg-turbo8-dev \
#         libtiff5-dev \
#         libopenjp2-7-dev \
#         zlib1g-dev \
#         libfreetype6-dev \
#         liblcms2-dev \
#         libwebp-dev \
#         tcl8.6-dev \
#         tk8.6-dev \
#         python3-tk \
#         python3 \
#         python3-pip \
#         redis-server

# RUN useradd -ms /bin/bash frappe

# USER frappe


# # Install NVM
# ENV NVM_DIR /home/frappe/.nvm
# ENV NODE_VERSION 18.19.0
# ENV PATH="/home/frappe/.local/bin:${PATH}"
# ENV DIR="/home/frappe/frappe-bench"

# RUN mkdir -p $NVM_DIR && \
#     curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash && \
#     . $NVM_DIR/nvm.sh && \
#     nvm install $NODE_VERSION && \
#     nvm alias default $NODE_VERSION && \
#     nvm use default

# # Install Frappe and ERPNext
# RUN git clone https://github.com/frappe/bench.git /tmp/bench && \
#     pip install --upgrade /tmp/bench
   

# #COPY /app /home/frappe/frappe-bench/

# WORKDIR /home/frappe

# COPY ./myapp /home/frappe/frappe-bench/

# VOLUME [ "/home/frappe/frappe-bench/site" \
#         "/home/frappe/frappe-bench/sites/assents", \
#         "/home/frappe/frappe-bench/logs" \
#  ]



# # Expose the necessary ports for web, Frappe, and MySQL
# EXPOSE 80 8000 3306

# RUN chown -R frappe:frappe /home/frappe/

# #run comand docker run -it --entrypoint --name some-name -p 8001:80  -d image name
# # Set the entry point
# CMD ["bench", "start"]